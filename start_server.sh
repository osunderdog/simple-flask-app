
##SEE /usr/libexec/s2i/run
## https://github.com/sclorg/s2i-python-container/tree/master/3.6/s2i/bin
##
## or it may be appropriate to overload run, assemble and environment scripts...
#
# http://flask.pocoo.org/docs/0.12/quickstart/
#
# flask will depend on environment variables:
# FLASK_APP={python script containing flask engine}
# FLASK_DEBUG=1|0 Depending on need.
#
# Wondering about host... --host=0.0.0.0
#
flask run --host=0.0.0.0 --port=8080
